//
//  ContentView.swift
//  CupCakeCorner
//
//  Created by Leadconsultant on 11/13/19.
//  Copyright © 2019 Leadconsultant. All rights reserved.
//

import SwiftUI

class User :ObservableObject, Codable {
    
    //This tells these are the feilds that are going to be Coding Compliant
    enum codingKeys:CodingKey{
        case name
    }
    
    @Published var name = "Vadiraj Hippargi"
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: codingKeys.self)
        name = try container.decode(String.self , forKey: .name)
    }
    
    func encode(to encoder: Encoder) throws {
          var container = encoder.container(keyedBy: codingKeys.self)
            try container.encode(name, forKey: .name)
    }
}

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
